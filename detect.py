import cv2
import numpy as np
import pytesseract
from PIL import Image
# save รูปไว้ที่ไหน
text_file = open(r'E:\end\price tag\us\output.txt','w' )
# import image ดึงรูปจากไหน
image = cv2.imread('31.png')

gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) 
# cv2.imshow('gray', gray) 
cv2.waitKey(0) 

#binary 
ret,thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV) 
# cv2.imshow('second', thresh) 
cv2.waitKey(0) 

#dilation 
kernel = np.ones((10,20), np.uint8) 
kernel2 = np.ones((5,5),np.uint8)
erosion = cv2.erode(thresh,kernel2,iterations = 1)
# cv2.imshow('erosion', erosion) 
cv2.waitKey(0)
img_dilation = cv2.dilate(erosion, kernel, iterations=1) 
# cv2.imshow('dilated', img_dilation) 
cv2.waitKey(0)


#find contours 
im2, ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
#sort contours 
sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])

for i, ctr in enumerate(sorted_ctrs): 
    # Get bounding box 
    x, y, w, h = cv2.boundingRect(ctr) 
    
    # Getting ROI 
    roi = image[y:y+h, x:x+w] 
    # show ROI 
    #cv2.imshow('segment no:'+str(i),roi) 
    cv2.rectangle(image,(x,y),( x + w, y + h ),(0,255,0),2) 
    #cv2.waitKey(0) 
    # if w > 15 and h > 15: 
        # cv2.imwrite('{}.jng'.format(i), roi)
    grayImage = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 127, 255, cv2.THRESH_BINARY)
    kernel2 = np.ones((5,5),np.uint8)
    dilation = cv2.dilate(blackAndWhiteImage,kernel2,iterations = 1)
    text=pytesseract.image_to_string(dilation)
    print(text) 
    text_file.write(str(text)+"\n")	

text_file.close()
cv2.imshow('marked areas', image)
cv2.waitKey(0)


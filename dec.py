from flask import Flask
from flask_restful import Resource,Api,reqparse
from flask_cors import CORS
import werkzeug, os
import base64
import cv2
import numpy as np
import pytesseract
from PIL import Image

app = Flask(__name__)
api = Api(app)
CORS(app)

UPLOAD_FOLDER = 'static/img'
parser = reqparse.RequestParser()
#parser.add_argument('file',type=werkzeug.datastructures.FileStorage, location='files')
parser.add_argument('filename')
parser.add_argument('file')
class DetectLot(Resource):
        decorators=[]

        def post(self):
            data = parser.parse_args()
            #print(data)
            if data['file'] == "":
                return {'data':'','message':'No file found','status':'error'}
            photo = data['file']
            if photo:
                imgdata = base64.b64decode(photo)
                filename = data['filename']
                print(filename)
	    try:
		print("save image")
		img = open('static/img/'+filename,"wb")
		image.write(imgdata)
		image.close()
	    except:
		print("error upload")
                #photo.save(os.path.join(UPLOAD_FOLDER,filename))
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY) 
                cv2.waitKey(0) 
                ret,thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV) 
                cv2.waitKey(0)  
                kernel = np.ones((10,20), np.uint8) 
                kernel2 = np.ones((5,5),np.uint8)
                erosion = cv2.erode(thresh,kernel2,iterations = 1)
                cv2.waitKey(0)
                img_dilation = cv2.dilate(erosion, kernel, iterations=1) 
                cv2.waitKey(0)
                im2,ctrs = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                for i, ctr in enumerate(sorted_ctrs): 
                    x, y, w, h = cv2.boundingRect(ctr) 
                    roi = image[y:y+h, x:x+w] 
                    cv2.rectangle(img,(x,y),( x + w, y + h ),(0,255,0),2) 
                    grayImage = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                    (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 127, 255, cv2.THRESH_BINARY)
                    kernel2 = np.ones((5,5),np.uint8)
                    dilation = cv2.dilate(blackAndWhiteImage,kernel2,iterations = 1)
                    text=pytesseract.image_to_string(dilation)
                    print(text) 
                    cv2.waitKey(0)
                    if text:
                        return {'text':text,'message':'photo uploaded','status':'success'}
                    return {'text':'','message':'Something when wrong','status':'error'}
class HelloWorld(Resource):
	def get(self):
		print("hello im detect lot.")
		return {'data':'Hello i am Detect lot!'}
api.add_resource(HelloWorld,'/')
api.add_resource(DetectLot,'/detectlot')
if __name__ == '__main__':
	app.run(host='0.0.0.0',port=5000,debug=True)
